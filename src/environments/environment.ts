// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyACSSfEcX9S0JVHS8rrb9h10jKN4rSa8z4",
    authDomain: "firechat-59506.firebaseapp.com",
    databaseURL: "https://firechat-59506.firebaseio.com",
    projectId: "firechat-59506",
    storageBucket: "",
    messagingSenderId: "943359785699"
}
};
